# phloxbot
A bot that posts art made using my [phlox](https://gitlab.com/Bleu-Box/phlox) gem to a [botsin.space account](https://botsin.space/@phlox).
